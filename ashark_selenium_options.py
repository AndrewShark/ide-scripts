#!/usr/bin/env python3

# Common settings for Ashark's selenium scripts.

# Install selenium and web-driver manager: `pip install selenium`, `pip install webdriver-manager`.

# Usage
# Start chromium/vivaldi `chromium --remote-debugging-port=9222`. Otherwise, use browser as normal.

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.core.utils import ChromeType
import ashark_selenium_crutches


def get_driver():
    chrome_options = Options()
    chrome_options.add_experimental_option("debuggerAddress", "127.0.0.1:9222")

    # https://stackoverflow.com/a/70088095/7869636 - Selenium connect to existing browser.
    # Need to start chromium as: chromium --remote-debugging-port=9222

    driver = webdriver.Chrome(service=Service(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM, version="106.0.5249.61").install()), options=chrome_options)
    #driver = webdriver.Chrome(service=Service(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install()), options=chrome_options)
    ashark_selenium_crutches.select_real_active_tab(driver)
    print("Working with tab called:", driver.title)

    return driver
