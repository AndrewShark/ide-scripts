# Andrew Shark IDE Scripts

## Use _current_ file in run configuration (hack)

Useful when you have lots of python files in one project, that are self-sufficient. For example, when you learn some course and each file is its own task solution.  
The script is needed because currently pycharm does not allow you to use macroses in script path. And it is annoying to manually point the python file each time you switch to next task.  
And running the current file with ctrl + shift + f10 is not a solution, because you cannot use redirect file for such ad-hoc run configurations.  
See https://youtrack.jetbrains.com/issue/IDEABKL-689

Two approaches exist. Choose one that you like more.

### General approach based on changing workspace.xml
`update_current_script_path.sh`
This script is used to change configuration to use currently opened python file.  
Places the current_python.py file path to the "Script path" field.  
For configuration in pycharm, see inside the script itself.

### Alternative approach based on symlinking files
`relink_runme_file.sh`  
This script is used to relink runme.py file to currently opened file.  
Makes symlink from runme.py -> current opened file  
For configuration in pycharm, see inside the script itself.

## Dynamically auto select the Redirect input file (hack)
Useful when you want each python file to have its own stdin input file, but do not want to select it manually for each python file you switch.  

Two approaches exist. Choose one that you like more.

### General approach based on changing workspace.xml
`update_redirect_input_file.sh`  
This script is used to change configuration to use the needed file as stdin input  
Places the current_python_basename.txt file path to the "Redirect input from" field.  
For configuration in pycharm, see inside the script itself.

### Alternative approach based on symlinking files
`relink_input_file.sh`  
This script is used to relink the file used as stdin input  
Makes symlink from input.txt -> current opened task.txt  
For configuration in pycharm, see inside the script itself.

## Automatically change the configuration name to include current file name when using approach with editing workspace.xml
`update_configuration_name.sh`  
This script is used to change configuration name, for your convenience, to the "current: "+ name of the currently launched python file name  
Places the "current: python_filename.py" to the "Name" of run/debug configuration.  

## Force use the current file at first switch to it
`run_expected_at_config_update.sh`  
This script forces expected behavior when changing current file and running it.  
Prevents execution of wrong file and relaunches run/debug configuration to use current file.  
For configuration in pycharm, see inside the script itself.

## Notes about differences in approaches

Consider following advantages and disadvantages in using edit_config approach instead remake symlinks approach.  
Advantages:
- When you start the file, then in tracebacks you see the normal filename you work with (and not the common file like runme.py). So you do not always mess with runme.py in tabs when you click to traces links.
- All breakpoints are stored for that individual file and not for the common one like runme.py.
- Not having mess with input.txt, always forgetting where it points and doing edits to wrong file.

Disadvantages:
- It always starts in debug mode first time, despite if you launched it in run of debug mode.

## Yandex Lyceum upload tool
`upload_to_yandex_lms.py`  
This tool is used to automatically upload current opened py file to yandex lms system. It requires selenium to be installed.  
For configuration in pycharm, see inside the script itself.

## Yandex Lyceum new task file pair tool
`create_new_lms_solution_files.py`  
This script is used as a tool in PyCharm to create a new task solution for yandex lyceum lms system. It requires selenium to be installed.  
For configuration in pycharm, see inside the script itself.

## Setup
Make a symlink to ashark_selenium_crutches.py from the site-packages dir:  
`ln -s /home/andrew/Development/ide-scripts/ashark_selenium_crutches.py ~/.local/lib/python3.10/site-packages/ashark_selenium_crutches.py`

Make a symlink to ashark_selenium_options.py from the site-packages dir:  
`ln -s /home/andrew/Development/ide-scripts/ashark_selenium_options.py ~/.local/lib/python3.10/site-packages/ashark_selenium_options.py`

## VS Codium

### Launch current file
There is no problem of launching current file. Use the following congiguration for `launch.json`:
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current file with input redirect",
            "type": "python",
            "request": "launch",
            "program": "${fileDirname}/${fileBasenameNoExtension}.py",
            "console": "integratedTerminal",
            "args": [ "<", "${fileDirname}/${fileBasenameNoExtension}.txt" ]
        }
    ],
}
```

This will even allow launching with txt file in focus.

### Toggle Breakpoint by Middle click

There is a problem currently that VSC does not support mouse shortcuts. See https://github.com/microsoft/vscode/issues/3130

But with this workaround you can still get (kinda) this functionality.

For setup, see https://stackoverflow.com/a/72297531/7869636
