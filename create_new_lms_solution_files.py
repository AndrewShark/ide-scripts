#!/usr/bin/env python3

# This script is used as a tool in PyCharm to create a new task solution for yandex lyceum lms system.
# Creates new taskname.py, taskname.txt and adds them to git. Taskname is taken from the lms web page.

# Setup in PyCharm:
# In File -> Settings -> Tools -> External Tool add a new tool (a plus symbol button) and name it "new lms task solution".
#   Program: point to this file (pycharm-scripts/create_new_lms_solution_files.py)
#   Arguments: "$FilePath$"
# In Appearance & Behavior -> Menus and Toolbars -> Navigation Bar Toolbar add a new Action (plus symbol button) -> External Tools -> new lms task solution (as an image you can use FileChooser.NewFile icon).

# Usage:
# Open the page with the task you want to solve (tool uploads to the opened tab page).
# In pycharm go to any file that is located in the same directory (lesson) as the new one will be in.
# Press the new lms solution button on the toolbar.
# The script will take the task name from the web page and create file pair for solution.
# For example, your lesson folder is called "03. If statements", previous task solution is called "02. Compare strings.py", current task is called "Compare numbers".
# Doing the instructions above will create "03. Compare numbers.py" and "03. Compare numbers.txt" inside the "03. If statements" directory, and files will be added to git.

import sys
import os
import subprocess
from selenium.webdriver.common.by import By
import ashark_selenium_options

driver = ashark_selenium_options.get_driver()

if "tasks" in driver.current_url:
    mode = "task"
else:
    mode = "newlesson"

task_name_el = driver.find_elements(By.CLASS_NAME, "heading_level_1")
if len(task_name_el) != 1 and len(task_name_el) != 2:
    print("unexpected number of task name headers: " + str(len(task_name_el)))
    exit(1)
task_name = task_name_el[0].text
filepath = sys.argv[1]
filedir = os.path.dirname(filepath)

if mode == "task":
    # Detect the next task number
    listOfFiles = [f for f in os.listdir(filedir) if f.endswith(".py")]
    listSorted = sorted(listOfFiles)
    if not listSorted:
        listSorted = ["00.py"]  # for taking 01 for new lessons without any py file yet.
        # Unfortunately, it is impossible to choose the empty folder in project pane as a parameter in macros.
        # Maybe pycharm plugin could communicate with pycharm to take that info?
    last_el = listSorted.pop()
    last_num = int(last_el[:2])
    next_num = last_num + 1
    next_num_str = str(next_num).zfill(2)

    task_path = filedir + '/' + next_num_str + ". " + task_name
    py_file = task_path + ".py"
    txt_file = task_path + ".txt"
    cur_lms_task_file = filedir + "/../" + ".current_lms_task.txt"  # This file is later used when opening window split layout (txt and py)

    with open(py_file, 'w') as f:
        f.write('#!/usr/bin/env python3\n\n')
    with open(txt_file, 'w') as f:
        f.write('')
    with open(cur_lms_task_file, 'w') as f:
        f.write(task_path)

    #p = subprocess.run(["git", "add", py_file], capture_output=True, cwd=filedir)
    #p = subprocess.run(["git", "add", txt_file], capture_output=True, cwd=filedir)
if mode == "newlesson":
    # Detect the next lesson number
    # listOfDirs = [f for f in os.listdir(filedir + "/..") if f[0].isdigit()]
    listOfDirs = [os.path.basename(filedir)]  # To support creating a new lesson between current and any followings
    listSorted = sorted(listOfDirs)
    
    last_el = listSorted.pop()
    last_num = int(last_el[:2])
    next_num = last_num + 1
    next_num_str = str(next_num).zfill(2)
    
    lesson_path = filedir + "/../" + next_num_str + ". " + task_name
    os.mkdir(lesson_path)

    with open(lesson_path + "/00. fake.py", 'w') as f:
        f.write('#!/usr/bin/env python3\n\n')
