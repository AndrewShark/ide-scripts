#!/bin/bash

# When middle click, if VSC window is active, send input to toggle breakpoint. If not VSC window, passthrough middle click.
# This is a crutch until https://github.com/microsoft/vscode/issues/3130 is fixed.
# For detailed setup instructions see Readme.

class=$(xdotool getactivewindow getwindowclassname)

if [ "x$class" == "xVSCodium" ]; then
    ydotool click 0xC1  # right click
    sleep 0.2
    ydotool key 107:1 107:0 28:1 28:0  # End, Enter
else
    ydotool click 0xC2  # middle click
fi
