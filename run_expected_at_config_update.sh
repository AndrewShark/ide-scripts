#!/bin/bash

# This script forces expected behavior when changing current file and running it.
# Prevents execution of wrong file and relaunches run/debug configuration to use current file.

# When switching to another python file and launching it, you end up with running previous python file. This is because changing configuration happens when it is currently running, so pycharm still thinks that the python file to run is that it was before changing. The second launch is ok.
# To prevent such behavior, this script is used. It stops execution of current config if it detects that it was changed, and invokes relaunch command. So from the users perspective of view, all works just as expected.

# Setup in PyCharm:
# In Run/Debug configuration called "current:" (here name is important (not just for your use), it should start with curr.):
# In External Tool create tool named "run expected at config update":
#   Program: point to this file (pycharm-scripts/run_expected_at_config_update.sh)
#   Arguments: "$ModuleFileDir$"
# Do not forget to add created tool (run expected at config update) to the Before launch list.
# This tool should go _after_ the update tools you use (update current script name, update redirect input file, update configuration name).

idea_dir="$1"

proceed_state_file="$idea_dir/.proceed"

should_proceed="`cat "$proceed_state_file"`"  # This value is set by update_current_script_path.sh script
echo "Reading should proceed value from $proceed_state_file. It says: $should_proceed"
if [ "$should_proceed" == "yes" ]; then 
    echo "Okey, proceeding"
    exit 0
else
    echo "Running background command that will relaunch run config after we kill ourself"
    $(sleep 0.5; wmctrl -x -a "jetbrains-pycharm.jetbrains-pycharm"; xdotool key "shift+F9") &    
    echo "Stopping."
    exit 1
fi
