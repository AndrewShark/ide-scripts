#!/usr/bin/env python3

# This script is used as a tool in PyCharm to upload current opened python file to the yandex lyceum lms system.
# It programmatically drops the file to the upload button in lms web page.

# Install: `pip install pycodestyle`

# Setup in PyCharm:
# In File -> Settings -> Tools -> External Tool add a new tool (a plus symbol button) and name it "lms upload".
#   Program: point to this file (pycharm-scripts/upload_to_yandex_lms.py)
#   Arguments: "$FilePath$"
# In Appearance & Behavior -> Menus and Toolbars -> Navigation Bar Toolbar add a new Action (plus symbol button) -> External Tools -> lms upload (use lms upload.png from image folder).

# Usage:
# Open the page with the task you solved (tool uploads to the opened tab page).
# In pycharm go to the file you are ready to upload.
# Press the lms upload button on the toolbar.
# The script will send the file to the web page.

from selenium.webdriver.common.by import By
import sys
import os
import time
import ashark_selenium_options
import subprocess

filepath = sys.argv[1]
alt_filepath = filepath.replace(".txt", ".py")
if not filepath.endswith(".py"):
    if os.path.isfile(alt_filepath):
        print("You selected \""+ filepath + "\". Assuming you wanted to choose \"" + alt_filepath + "\".")
        filepath = alt_filepath
    else:
        print("Not a python file, refusing to send")
        exit(1)

time.sleep(1)

p = subprocess.run(os.path.expanduser("~/.local/bin/") + "pycodestyle --max-line-length=120 \"" + filepath + "\"", shell=True)
if p.returncode == 1:
    print("Failed to pass pycodestyle")
    exit(1)

driver = ashark_selenium_options.get_driver()

# upload_button = driver.find_element(By.ID, "xuniq1")  # Unreliable, because that last number 1 changes to another numbers
upload_button = driver.find_elements(By.CLASS_NAME, "Attach-Control")
if len(upload_button) != 1:
    print("unexpected number of input buttons: " + str(len(upload_button)))
    exit(1)

upload_button[0].send_keys(filepath)
print("Finished")
