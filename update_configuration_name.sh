#!/bin/bash

# This script is used to change configuration name, for your convenience, to the "current: "+ name of the currently launched python file name
# Places the "current: python_filename.py" to the "Name" of run/debug configuration.

# Setup in PyCharm:
# In Run/Debug configuration called "current:" (here name is important (not just for your use), it should start with curr.):
# In External Tool create tool named "update configuration name":
#   Program: point to this file (pycharm-scripts/update_configuration_name.sh)
#   Arguments: "$ModuleFileDir$" "$FilePathRelativeToProjectRoot$"
#   Working directory: $FileDir$
# Do not forget to add created tool (update configuration name) to the Before launch list.

idea_dir="$1"
cur_file_from_project="$2"
filebase="${filename%.*}"

echo "$idea_dir $cur_file_from_project"

if [ ! -d "$idea_dir" ]; then
    echo "idea directory does not exist" >&2
    exit 1
elif [ ! -f "$idea_dir/workspace.xml"  ]; then
    echo "workspace.xml does not exist" >&2
    exit 1 
fi

filename=$(basename -- "$cur_file_from_project")
extension="${filename##*.}"
filebase="${filename%.*}"

if [ "$extension" != "py" ]; then
    if [ -f "$filebase".py ]; then
        echo "You selected \"$filename\". Assuming you wanted to choose \"$filebase.py\"."
        filename="$filebase".py
    else
        echo "Not a python file, exitting"
        exit 1
    fi
fi

echo ready
newval="current: $filename"
echo "Setting new value: $newval"
xmlstarlet ed --inplace -u "project/component[@name='RunManager']/configuration[starts-with(@name, 'curr')]/@name" -v "$newval" "$idea_dir/workspace.xml"
echo finished
