#!/usr/bin/env python3

# Ashark selenium crutches
# This script allows you to switch selenium web driver to currently opened tab in real GUI.
# There is an issue of unablility to do that out of the box. See https://stackoverflow.com/a/71857267/7869636 (not a proper bug report, but explains in more details).
# Use this script as a module in other selenium scripts.

# Install pywmctrl: `pip install wmctrl`

from wmctrl import Window
import sys
import subprocess

# Unfortunately, this simple solution not always work:
# driver.switch_to.window(driver.window_handles[0])  # switch driver to currently opened tab
# So need to use one of the crutches


def get_real_active_tab_x11():
    all_x11_windows = Window.list()
    browser_class="vivaldi-snapshot.Vivaldi-snapshot"
    # browser_class='chromium.Chromium'
    # browser_class="google-chrome.Google-chrome"
    browser_os_windows = [el for el in all_x11_windows if el.wm_class == browser_class]
    if len(browser_os_windows) != 1:
        print("Unexpected number of " + browser_class + " windows", file=sys.stderr)
        exit(1)
    # return browser_os_windows[0].wm_name.rstrip(" – Chromium")
    # return browser_os_windows[0].wm_name.rstrip(" - Google Chrome")
    return browser_os_windows[0].wm_name.rstrip(" - Vivaldi")


def get_real_active_tab_wayland():
    all_wayland_windows = subprocess.run("python ~/bin/get_list_of_windows", shell=True, capture_output=True).stdout.decode().rstrip().split("\n")
    opened_tab = ""  # this script is part of Ashark-bin
    for window in all_wayland_windows:
        #if not window.endswith("Vivaldi"):
        if not window.endswith("Chromium"):
            continue
        #opened_tab = window.rstrip(" - Vivaldi")
        opened_tab = window.rstrip(" - Chromium")
    return opened_tab


def select_real_active_tab(driver):
    # real_active_tab_name = get_real_active_tab_x11()
    real_active_tab_name = get_real_active_tab_wayland()

    tabs = driver.window_handles
    found_active_tab = False
    for tab in tabs:
        driver.switch_to.window(tab)
        if driver.title == real_active_tab_name:
            found_active_tab = True
            break

    if not found_active_tab:
        print("Cannot switch to needed tab, something went wrong.\nSearched for " + real_active_tab_name, file=sys.stderr)
        exit(1)
    else:
        print("Successfully switched to opened tab")
